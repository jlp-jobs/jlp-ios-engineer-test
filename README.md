# JLP iOS Engineer Test

## Brief

Your task is to create an iOS App that will allow customers to see the range of dishwashers on sale at John Lewis. This app should be simple to use and work on both iPhones and iPads.

You may choose to use the existing project or create your own.

You should build the application in Swift.

You are expected to provide adequate unit tests.

## Project

An example base project has been set up for you that uses Storyboards and has Unit and UI test schemes. You may choose to use this, or create your own.

## Product Grid

On initial app launch the customer should be presented with a grid of dishwashers available for purchase at John Lewis.

## Product Page

When a dishwasher is tapped on the product grid a new screen is displayed that displays the details about that particular product.

## Data

You can call these APIs:
#### Main List Feed
`https://gitlab.com/jlp-jobs/jlp-ios-engineer-test/-/raw/main/mockData/data.json?ref_type=heads`

#### Example Product Detail
`https://gitlab.com/jlp-jobs/jlp-ios-engineer-test/-/raw/main/mockData/data2.json?ref_type=heads`

or use the mock data provided in `/mockdata`

## Designs

In the `/designs` folder you can find example designs of how the pages might look:
* product-page-compact.png
* product-page-regular.png
* product-grid.png

## Access Instructions 

Please read the instructions in th README document carefully before starting. If you have any questions, or need some clarification around the requirements of the test, please do not hesitate to reply directly to the email you were sent. There is no specific deadline as we appreciate how busy life can be.

Once you have a user setup in GitLab, please reply to the email you were sent with your Gitlab username - we need this to add you to the correct user group on our end. 

After that you're free to fork the repo and have some fun with it.  Please however make sure your repo is private to keep it fair for the other candidates. 

Once your submission is complete, and we’ve added you to the group on our end, please grant developer access to the jlp-test-review/candidate GitLab group so we can see your code. Let us know when you’re done and one of our senior engineers will review your submission, and I’ll let you know the outcome as soon as possible.



## Things we’re looking for:
* Unit tests are important. We’d like to see a TDD approach to writing the app.
* This app should be simple to use and work on both iPhones and iPads. We've provided you with some images in the designs folder as a guide.
* The use of third party code/SDKs is allowed, but you should be able to explain at interview why you have chosen the third party code.
* We’re looking for a solution that's as close to the designs as possible.
* We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
* We don't expect you to spend too long on this, as a guide 3 hours is usually enough.
* You will need to use Swift for this test.
* Put all your assumptions, notes, instructions and improvement suggestions into your GitHub README.md.
