#Pairing Test

## Bedrock Framework

Bedrock is a packaged framework for standard library and foundation extensions.

## Notes

Reusable standard library and foundation extensions should be added here. 

Do not make this framework depend on any other components.

When building or running code please use `Bedrock-Package` scheme

## Exercise

You will be given 5 minutes to familiarise yourself with the Package and run any activities you wish.

The exercise will involve some questions about:
- what are your first thoughts as a quality engineer about the state of this package
- what improvements would you suggest to make to this package
- carry out some or all of these improvements
