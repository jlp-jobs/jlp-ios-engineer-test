//
// Copyright © 2019 John Lewis plc. All rights reserved.
//

import Bedrock
import XCTest

// 🙂
// Use https://www.epochconverter.com to validate time intervals.
// Date variables are in the format _HH_mm__dd_MM_yyyy_ZZZ for your enjoyment.

final class CalendarNextDateTests: XCTestCase {

    func testGMT() throws {

        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "GMT")!

        let components = DateComponents(hour: 12, minute: 30)
        let _00_00__13_05_2019_GMT = Date(timeIntervalSince1970: 1557705600)
        let _00_00__15_05_2019_GMT = Date(timeIntervalSince1970: 1557878400)
        let _12_30__13_05_2019_GMT = Date(timeIntervalSince1970: 1557750600)

        let range = Range(from: _00_00__13_05_2019_GMT, to: _00_00__15_05_2019_GMT)
        let date =  calendar.nextDate(in: range, matching: components)

        XCTAssertEqual(date, _12_30__13_05_2019_GMT)
    }

    func testBST() throws {

        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "BST")!

        let components = DateComponents(hour: 12, minute: 30)
        let _00_00__13_05_2019_BST = Date(timeIntervalSince1970: 1557702000)
        let _00_00__15_05_2019_BST = Date(timeIntervalSince1970: 1557874800)
        let _12_30__13_05_2019_BST = Date(timeIntervalSince1970: 1557747000)

        let range = Range(from: _00_00__13_05_2019_BST, to: _00_00__15_05_2019_BST)
        let date =  calendar.nextDate(in: range, matching: components)

        XCTAssertEqual(date, _12_30__13_05_2019_BST)
    }

    func testTimeIsOutOfRange() throws {

        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "GMT")!

        let components = DateComponents(hour: 12, minute: 30)
        let _13_00__13_05_2019_GMT = Date(timeIntervalSince1970: 1557752400)
        let _00_00__14_05_2019_GMT = Date(timeIntervalSince1970: 1557792000)

        let range = Range(from: _13_00__13_05_2019_GMT, to: _00_00__14_05_2019_GMT)
        let date =  calendar.nextDate(in: range, matching: components)

        XCTAssertNil(date)
    }

    func testStartEndIsTheSame() throws {

        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "GMT")!

        let components = DateComponents(hour: 12, minute: 30)
        let _13_00__13_05_2019_GMT = Date(timeIntervalSince1970: 1557752400)

        let range = Range(from: _13_00__13_05_2019_GMT, to: _13_00__13_05_2019_GMT)
        let date =  calendar.nextDate(in: range, matching: components)

        XCTAssertNil(date)
    }

    func testStartDateIsResult() throws {

        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "GMT")!

        let components = DateComponents(hour: 12, minute: 30)
        let _12_30__13_05_2019_GMT = Date(timeIntervalSince1970: 1557750600)
        let _00_00__15_05_2019_GMT = Date(timeIntervalSince1970: 1557878400)

        let range = Range(from: _12_30__13_05_2019_GMT, to: _00_00__15_05_2019_GMT)
        let date =  calendar.nextDate(in: range, matching: components)

        XCTAssertEqual(date, _12_30__13_05_2019_GMT)
    }
}

extension Range where Bound == Date {

    /// Creates a range from the given dates.
    ///
    /// Makes dates easier to read in the tests.
    ///
    /// - Parameters:
    ///   - start: The lowerBound
    ///   - end: The upperBound
    fileprivate init(from start: Date, to end: Date) {
        self = start..<end
    }
}
