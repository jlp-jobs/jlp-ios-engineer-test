//
// Copyright © 2019 John Lewis plc. All rights reserved.
//

import Foundation
import XCTest

// swiftlint:disable capitalized_method

/// An assertion that checks that the expression throws an error equal to the
/// given error.
///
/// - Parameters:
///   - expression: An expression that can throw an error.
///   - expectedError: The error that is expected to be thrown.
///   - file: The file in which failure occurred. Defaults to the file name of
///           the test case in which this function was called.
///   - line: The line number on which failure occurred. Defaults to the line
///           number on which this function was called.
public func AssertThrows<T, E>(
	_ expression: @autoclosure () throws -> T,
	_ expectedError: E,
	file: StaticString = #file,
	line: UInt = #line
) where E: Swift.Error, E: Equatable {

	XCTAssertThrowsError(try expression(), file: file, line: line) { error in

		guard let typedError = error as? E else {
			XCTFail("Thrown error \(error) is not of type \(expectedError.self)", file: file, line: line)
			return
		}

		XCTAssertEqual(typedError, expectedError, file: file, line: line)
	}
}

public func AssertThrows<T, E>(
	_ expression: @autoclosure () throws -> T,
	_ type: E.Type,
    _ expectation: @escaping (E) -> Bool = { _ in true },
	file: StaticString = #file,
	line: UInt = #line
) where E: Swift.Error {

	XCTAssertThrowsError(try expression(), file: file, line: line) { error in

		guard let typedError = error as? E else {
			XCTFail("Thrown error \(error) is not of type \(type)", file: file, line: line)
			return
		}

		XCTAssertTrue(expectation(typedError), "Thrown error \(typedError) is not correct", file: file, line: line)
	}
}

// swiftlint:enable capitalized_method
