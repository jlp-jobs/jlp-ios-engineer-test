//
// Copyright © 2019 John Lewis plc. All rights reserved.
//

import Foundation
import XCTest

// swiftlint:disable capitalized_method

/// Asserts that a result is a success.
///
/// - Parameters:
///   - result: Bedrock Result.
///   - validation: A function to validate the result value.
///   - file: The file in which failure occurred. Defaults to the file name of the test case in which this function was called.
///   - line: The line number on which failure occurred. Defaults to the line number on which this function was called.
public func AssertSuccess<Success, Failure: Swift.Error>(
    _ result: Result<Success, Failure>,
    _ validation: (Success) -> Void = { _ in },
    file: StaticString = #file,
    line: UInt = #line
    ) {

    switch result {
    case .failure(let error):
        XCTFail("Didn't expect failure: \(error)", file: file, line: line)
    case .success(let value):
        XCTAssertNotNil(value)
        validation(value)
    }
}

/// Asserts that a result is a failure.
///
/// - Parameters:
///   - result: Bedrock Result.
///   - validation: A function to validate the result value.
///   - file: The file in which failure occurred. Defaults to the file name of the test case in which this function was called.
///   - line: The line number on which failure occurred. Defaults to the line number on which this function was called.
public func AssertFailure<Success, Failure: Swift.Error>(
    _ result: Result<Success, Failure>,
    _ validation: (Failure) -> Void = { _ in },
    file: StaticString = #file,
    line: UInt = #line
    ) {

    switch result {
    case .failure(let error):
        XCTAssertNotNil(error)
        validation(error)

    case .success(let value):
        XCTFail("Didn't expect success: \(value)", file: file, line: line)
    }
}

// swiftlint:enable capitalized_method
