//
// Copyright © 2018 John Lewis plc. All rights reserved.
//

import Bedrock
import Foundation
import XCTest

final class StringReplacingCharacterSetTests: XCTestCase {

	func testReplace() {
		let string = "This\nis\na\nstring"
		let new = string.replacingOccurrences(of: CharacterSet.newlines, with: "NEWLINE")
		XCTAssertEqual(new, "ThisNEWLINEisNEWLINEaNEWLINEstring")
	}
}
