//
// Copyright © 2018 John Lewis plc. All rights reserved.
//

import Bedrock
import XCTest

class DateFormatterExtensionsTests: XCTestCase {

    func test_string_dateFormatOne() throws {
        
        let date = try XCTUnwrap(createDate())
        let dateFormatter = DateFormatter(format: .short)
        let dateString = dateFormatter.string(from: date)
        
        XCTAssertEqual(dateString, "21 Oct 2017")
    }
    
    func test_string_dateFormatTwo() throws {
        
        let date = try XCTUnwrap(createDate())
        let dateFormatter = DateFormatter(format: .medium)
        let dateString = dateFormatter.string(from: date)
        
        XCTAssertEqual(dateString, "21 October 2017")
    }
    
    func test_string_dateFormatThree() throws {
        
        let date = try XCTUnwrap(createDate())
        let dateFormatter = DateFormatter(format: .long)
        let dateString = dateFormatter.string(from: date)
        
        XCTAssertEqual(dateString, "Saturday 21 October")
    }
    
    func test_string_dateFormatFour() throws {
        
        let date = try XCTUnwrap(createDate())
        let dateFormatter = DateFormatter(format: .descriptive)
        let dateString = dateFormatter.string(from: date)
        
        XCTAssertEqual(dateString, "21 Oct 2017 at 17:25")
    }
    
    func test_string_dateFormatFive() throws {
        
        let date = try XCTUnwrap(createDate())
        let dateFormatter = DateFormatter(format: .time)
        let dateString = dateFormatter.string(from: date)
        
        XCTAssertEqual(dateString, "5pm")
    }
}

extension DateFormatterExtensionsTests {
    
    func createDate() -> Date? {
        
        let dateComponents = DateComponents(calendar: .current,
                                            timeZone: .current,
                                            year: 2017,
                                            month: 10,
                                            day: 21,
                                            hour: 17,
                                            minute: 25,
                                            second: 08)
        
        return dateComponents.date
    }
}
