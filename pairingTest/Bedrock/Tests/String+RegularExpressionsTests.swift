//
// Copyright © 2021 John Lewis plc. All rights reserved.
//

@testable import Bedrock
import XCTest

class String_RegexTests: XCTestCase {
    
    // MARK: - validatePhoneNumber
    func test_validatesPhoneNumber() {
        let number = "01234567890"
        XCTAssertEqual(number.validatePhoneNumber(), .valid)
    }
    
    func test_validatesPhoneNumber_internationalAccepted() {
        let number = "+441234567890"
        XCTAssertEqual(number.validatePhoneNumber(), .valid)
    }
    
    func test_validatesPhoneNumber_NAN() {
        let number = "Not a number"
        XCTAssertEqual(number.validatePhoneNumber(), .invalid("Please enter a phone number containing only numeric characters and +"))
    }
    
    func test_validatesPhoneNumber_tooShort() {
        let number = "01234"
        XCTAssertEqual(number.validatePhoneNumber(), .invalid("Please enter a phone number with at least 11 digits"))
    }
    
    func test_validatesPhoneNumber_tooLong() {
        let number = "0123456789001234578091231240989532"
        XCTAssertEqual(number.validatePhoneNumber(), .invalid("Please enter a phone number with no more than 15 digits"))
    }
    
    // MARK: - isUKPostcode
    func test_isUKPostcode() {
        let postcode = "SW1E 5NN"
        XCTAssertTrue(postcode.isUKPostcode())
    }
    
    func test_isUKPostcode_whenNoCharacters_isInvalid() {
        let postcode = "12345"
        XCTAssertFalse(postcode.isUKPostcode())
    }
    
    func test_isUKPostcode_whenEmpty_isInvalid() {
        let postcode = ""
        XCTAssertFalse(postcode.isUKPostcode())
    }
}
