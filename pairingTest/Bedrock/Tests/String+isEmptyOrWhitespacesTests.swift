//
// Copyright © 2018 John Lewis plc. All rights reserved.
//

import Bedrock
import XCTest

final class StringIsEmptyOrWhitespaceTests: XCTestCase {

    func test_whenStringIsEmpty() {
        XCTAssertTrue("".isEmptyOrWhitespaces())
    }
    
    func test_whenStringHasWhiteSpaceOnly() {
        XCTAssertTrue(" ".isEmptyOrWhitespaces())
    }
    
    func test_whenStringStartWithWhitespace() {
        XCTAssertFalse(" abc".isEmptyOrWhitespaces())
    }
    
    func test_whenStringEndtWithWhitespace() {
        XCTAssertFalse("abc ".isEmptyOrWhitespaces())
    }
    
    func test_whenStringHasNewLineOnly() {
        XCTAssertTrue("\n".isEmptyOrWhitespaces())
    }
    
    func test_whenStringOnlyHasWhitespaceAndNewLine() {
        XCTAssertTrue(" \n".isEmptyOrWhitespaces())
    }
    
    func test_whenStringEndWithNewLine() {
        XCTAssertFalse("abc\n".isEmptyOrWhitespaces())
    }
    
    func test_whenStringHasWhitespaceAndNewLine() {
        XCTAssertFalse(" abc\n".isEmptyOrWhitespaces())
    }
}
