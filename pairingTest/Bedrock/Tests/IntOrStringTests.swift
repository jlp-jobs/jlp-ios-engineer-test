//
// Copyright © 2021 John Lewis plc. All rights reserved.
//

import Bedrock
import XCTest

final class IntOrStringTests: XCTestCase {

    func testInitInt() {
        XCTAssertEqual(IntOrString(4), .int(4))
    }

    func testExpressibleByIntegerLiteral() {
        let value: IntOrString = 3
        XCTAssertEqual(value, .int(3))
    }

    func testInitString() {
        XCTAssertEqual(IntOrString("A"), .string("A"))
    }

    func testExpressibleByStringLiteral() {
        let value: IntOrString = "A"
        XCTAssertEqual(value, .string("A"))
    }

    func testCustomStringConvertible() {
        XCTAssertEqual(IntOrString.int(1234).description, "1234")
        XCTAssertEqual(IntOrString.string("ABCD").description, "ABCD")
    }

    struct Item: Codable, Equatable {
        let id: IntOrString
    }

    func testDecodableInt() throws {
        let json = #"{ "id": 1234 }"#
        let data = try XCTUnwrap(json.data(using: .utf8))
        let item = try JSONDecoder().decode(Item.self, from: data)
        XCTAssertEqual(item, Item(id: 1234))
    }

    func testDecodableString() throws {
        let json = #"{ "id": "1234" }"#
        let data = try XCTUnwrap(json.data(using: .utf8))
        let item = try JSONDecoder().decode(Item.self, from: data)
        XCTAssertEqual(item, Item(id: "1234"))
    }

    func testEncodableInt() throws {
        let item = Item(id: 1234)
        let data = try JSONEncoder().encode(item)
        let string = try XCTUnwrap(String(data: data, encoding: .utf8))
        XCTAssertEqual(string, #"{"id":1234}"#)
    }

    func testEncodableString() throws {
        let item = Item(id: "1234")
        let data = try JSONEncoder().encode(item)
        let string = try XCTUnwrap(String(data: data, encoding: .utf8))
        XCTAssertEqual(string, #"{"id":"1234"}"#)
    }
}
