
// swiftlint:disable capitalized_method

/// Provides a function that applies one transform then another transform.
///
/// The types T, U and V can be the same type if required, for instance if
/// applying two URLRequest transform functions in the form
/// `(URLRequest) -> URLRequest`.
///
/// - Parameters:
///   - transform1: The first transform to apply.
///   - transform2: The second transform to apply.
/// - Returns: A function which applies the two provided transform functions.
public func +<T, U, V> (
    _ transform1: @escaping (T) -> U,
    _ transform2: @escaping (U) -> V
) -> (T) -> V {
    return { t in
        let u = transform1(t)
        let v = transform2(u)
        return v
    }
}

public func +<T, U, V, F> (
    _ transform1: @escaping (T) -> Result<U, F>,
    _ transform2: @escaping (U) -> V
) -> (T) -> Result<V, F> {
    { transform1($0).map(transform2) }
}

public func +<T, U, V, F> (
    _ transform1: @escaping (T) -> Result<U, F>,
    _ transform2: @escaping (U) -> Result<V, F>
) -> (T) -> Result<V, F> {
    { transform1($0).flatMap(transform2) }
}

public func ||<T, U, F> (
    _ transform1: @escaping (T) -> Result<U, F>,
    _ transform2: @escaping (T) -> Result<U, F>
) -> (T) -> Result<U, F> {
    { t in
        switch transform1(t) {
        case .success(let u): return .success(u)
        case .failure: return transform2(t)
        }
    }
}
// swiftlint:enable capitalized_method
