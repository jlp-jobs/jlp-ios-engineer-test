import Foundation

public enum IntOrString: Equatable, Hashable {
    case int(Int)
    case string(String)
}

extension IntOrString: ExpressibleByIntegerLiteral {
    public init(_ value: Int) {
        self = .int(value)
    }
    
    public init(integerLiteral value: Int) {
        self = .int(value)
    }
}

extension IntOrString: ExpressibleByStringLiteral {
    public init(_ value: String) {
        self = .string(value)
    }
    
    public init(stringLiteral value: String) {
        self = .string(value)
    }
}

extension IntOrString: CustomStringConvertible {
    public var description: String {
        switch self {
        case let .int(value): return String(value)
        case let .string(value): return value
        }
    }
}

extension IntOrString: Codable {
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let value = try? container.decode(Int.self) {
            self = .int(value)
        } else {
            let value = try container.decode(String.self)
            self = .string(value)
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case let .int(value): try container.encode(value)
        case let .string(value): try container.encode(value)
        }
    }
}
