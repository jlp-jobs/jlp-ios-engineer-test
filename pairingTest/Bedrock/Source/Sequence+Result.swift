//
// Copyright © 2019 John Lewis plc. All rights reserved.
//

import Foundation

public extension Sequence {
    
    func results<Value>(_ transform: (Element) throws -> Value) -> [Result<Value, Swift.Error>] {
        
        return map { element in
            Result { try transform(element) }
        }
    }
}
