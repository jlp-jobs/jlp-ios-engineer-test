//
// Copyright © 2020 John Lewis plc. All rights reserved.
//

import Foundation

public struct IsEmptyError: Error {}

public struct NonEmpty<Element> {
    
    public let first: Element
    public var last: Element { tail.last ?? first }
    private let tail: [Element]
    
    private var array: [Element] { [first] + tail }
    
    public init(_ array: [Element]) throws {
        guard let first = array.first else {
            throw IsEmptyError()
        }
        self.first = first
        self.tail = Array(array.dropFirst())
    }
    
    public init(_ array: Element...) throws {
        
        guard let first = array.first else {
            throw IsEmptyError()
        }
        self.first = first
        self.tail = Array(array.dropFirst())
    }
}

extension NonEmpty: Sequence {
    
    public func makeIterator() -> Array<Element>.Iterator {
        return array.makeIterator()
    }
}

extension NonEmpty: Equatable where Element: Equatable {}
