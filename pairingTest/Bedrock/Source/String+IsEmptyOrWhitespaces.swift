//
// Copyright © 2018 John Lewis plc. All rights reserved.
//

import Foundation

extension String {
    
    public func isEmptyOrWhitespaces() -> Bool {
        return self.isEmpty || (self.trimmingCharacters(in: .whitespacesAndNewlines) == "")
    }
}
