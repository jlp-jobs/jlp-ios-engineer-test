//
// Copyright © 2018 John Lewis plc. All rights reserved.
//

extension String {

	/// The line separator character: 0x2028
	public static let lineSeparator = "\u{2028}"
}
