import Foundation

extension Date {
    public func add(days: Int) -> Date {
        return addingTimeInterval(TimeInterval(days * 24 * 60 * 60))
    }
    
    public func add(years: Int) -> Date {
        return addingTimeInterval(TimeInterval(years * 365 * 24 * 60 * 60))
    }
    
    public func dayDifference(until date: Date) -> Int? {
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: .day, in: .era, for: self) else {
            
            return nil
        }
        guard let end = currentCalendar.ordinality(of: .day, in: .era, for: date) else {
            
            return nil
        }
        return end - start
    }
    
    /// Has this date's epoch timestamp not reached the compared epoch timestamp.
    /// Epoch timestamps are not dependent on locale/timezone.
    public func isBefore(epochTimestamp compareTimestampInMilliSeconds: Int) -> Bool {
        let compareTimeIntervalSince1970 = Double(compareTimestampInMilliSeconds / 1000)
        return self.timeIntervalSince1970 < compareTimeIntervalSince1970
    }
}
