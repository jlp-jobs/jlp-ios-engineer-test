//
// Copyright © 2019 John Lewis plc. All rights reserved.
//

import Foundation

extension Calendar {

    /// Computes the next date which matches a given set of components in the
    /// given range.
    ///
    /// - Parameters:
    ///   - range: The range the returned date should lie in.
    ///   - components: The components to search for.
    /// - Returns: A Date representing the result of the search, or nil if a result could not be found.
    public func nextDate(in range: Range<Date>,
                         matching components: DateComponents) -> Date? {

        // Use the second before the lowerBound because the system nextDate
        // will find the next date _after_ the given date.
        let oneSecondBeforeLowerBound = range.lowerBound.addingTimeInterval(-1)

        guard let date = nextDate(after: oneSecondBeforeLowerBound,
                                  matching: components,
                                  matchingPolicy: .nextTime,
                                  repeatedTimePolicy: .first,
                                  direction: .forward) else {
            return nil
        }

        guard range.contains(date) else {
            return nil
        }

        return date
    }
}
