//
// Copyright © 2018 John Lewis plc. All rights reserved.
//

import Foundation

extension String {

	/// Returns a new string in which all occurrences of a character set in the
	/// receiver are replaced by a given string.
	///
	/// - Parameters:
	///   - characterSet: The character set from which to remove characters.
	///   - replacement: The string with which to replace with.
	/// - Returns: A new string in which all occurrences of the character set
	/// are replaced by the replacement string.
	public func replacingOccurrences(of characterSet: CharacterSet, with replacement: String) -> String {
		return components(separatedBy: characterSet).joined(separator: replacement)
	}
}
