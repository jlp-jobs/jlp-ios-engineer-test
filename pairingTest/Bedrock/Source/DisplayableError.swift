//
// Copyright © 2019 John Lewis plc. All rights reserved.
//

import Foundation

/// An error to be displayed to the user.
///
/// See UIAlertController+Error for more.
public struct DisplayableError: Error, Equatable {
    public static func == (lhs: DisplayableError, rhs: DisplayableError) -> Bool {
        lhs.title == rhs.title &&
        lhs.message == rhs.message &&
        lhs.actions.count == rhs.actions.count
    }
    
    /// Displayed as title on alert controller
    public var title: String
    
    /// Displayed as message on alert controller
    public var message: String?
    
    /// Actions to be associated with the alert controller
    public var actions: [Action]
    
    public init(title: String,
                message: String?,
                actions: [Action] = [.cancel]) {
        
        self.title = title
        self.message = message
        self.actions = actions
    }
}

extension DisplayableError.Action {
    
    /// A cancel action, with title "Cancel", style .cancel and
    /// handler which will dismiss the alert
    public static var cancel: DisplayableError.Action {
        
        return DisplayableError.Action(title: "Cancel",
                                       style: .cancel,
                                       handler: nil)
    }
    
    /// An okay action, with title "OK", style .default and
    /// handler which will dismiss the alert
    public static var ok: DisplayableError.Action {
        
        return DisplayableError.Action(title: "OK",
                                       style: .default,
                                       handler: nil)
    }
}

extension DisplayableError {
    
    /// An action to be associated with an alert
    public struct Action {
        
        public typealias Handler = () -> Void
        
        /// The title of the action's button
        public var title: String
        
        /// The style that is applied to the action's button, to map to a UIAlertAction's style, if an UIAlertController is used with the DisplayableError and its ctions
        public var style: Style = .default
        
        /// The style that is applied to the action's button,  to map to a Button's style, if an ErrorViewController is used with the DisplayableError and its actions
        public var buttonType: ButtonType?
        
        /// Associated handler / functionality of the button on UIAlertController or ErrorViewController
        public var handler: Handler?
        
        public init(title: String,
                    style: Style,
                    handler: Handler?) {
            
            self.title = title
            self.style = style
            self.handler = handler
        }
        
        public init(title: String,
                    buttonType: ButtonType,
                    handler: Handler?) {
            
            self.title = title
            self.buttonType = buttonType
            self.handler = handler
        }
    }
}

extension DisplayableError.Action {
    
    /// The style applied to the button of the action on alert controller
    ///
    /// - `default`: Apply default style
    /// - cancel: Apply a style that indicates the action cancels the operation
    ///           and leaves things unchanged.
    /// - destructive: Apply a style that indicates the action might change
    ///                or delete data
    public enum Style {
        
        case `default`
        
        case cancel
        
        case destructive
    }
    
    /// The style applied to the button on the ErrorViewController.
    /// These styles should map to the existing styles (ButtonStyle) of the Button component from Components module.
    public enum ButtonType {
        case primary
        case secondary
    }
}

extension DisplayableError {
    
    /// Returns a DisplayableError, containing a retry action which executes a function
    /// - Parameter retryFunction: A function to be retried when a retry action is executed
    public init(_ retryFunction: @escaping () -> Void ) {
        let retryAction = DisplayableError.Action(title: "Retry",
                                                  style: .default,
                                                  handler: retryFunction)
        let okAction =  DisplayableError.Action(title: "OK",
                                                style: .cancel,
                                                handler: nil)
        
        self.init(title: "Sorry, something went wrong",
                  message: "Please check your internet connection and try again.",
                  actions: [okAction, retryAction])
    }
    
    public init(okFunction: @escaping () -> Void, retryFunction: @escaping () -> Void ) {
        let retryAction = DisplayableError.Action(title: "Retry",
                                                  style: .default,
                                                  handler: retryFunction)
        let okAction =  DisplayableError.Action(title: "OK",
                                                style: .cancel,
                                                handler: okFunction)
        
        self.init(title: "Sorry, something went wrong",
                  message: "Please check your internet connection and try again.",
                  actions: [okAction, retryAction])
    }
}

public extension DisplayableError {
    static func timeout(actions: Action...) -> DisplayableError {
        return DisplayableError(
            title: "We've lost connection",
            message: "It may be your network",
            actions: actions)
    }
    
    static func generic(actions: Action...) -> DisplayableError {
        return DisplayableError(
            title: "Something went wrong",
            message: "We are having problems loading this page",
            actions: actions)
    }
}

extension DisplayableError.Action: Hashable {
    public static func == (lhs: DisplayableError.Action, rhs: DisplayableError.Action) -> Bool {
        lhs.title == rhs.title &&
        lhs.buttonType == rhs.buttonType &&
        lhs.style == rhs.style
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(title)
        hasher.combine(buttonType)
        hasher.combine(style)
    }
}
