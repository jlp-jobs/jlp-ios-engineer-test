//
// Copyright © 2019 John Lewis plc. All rights reserved.
//

import Foundation

// swiftlint:disable:next line_length
public var nameCharacterLimit = 80
public var phoneCharacterLimit = 15
public var phoneCharacterMinimum = 11
public var passwordCharacterLimit = 60
public var emailCharacterLimit = 40
public var emailRegex = try! NSRegularExpression(pattern: "^[\\w0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$", // swiftlint:disable:this line_length
    options: .caseInsensitive)
public var telephoneRegex = try! NSRegularExpression(pattern: "^[0-9+]{0,1}+[0-9]*$", options: .caseInsensitive)
public var postcodeRegex = try! NSRegularExpression(pattern: "([A-Za-z]{1,2}[0-9R][0-9A-Za-z]?)\\s*([0-9][A-Za-z-[CIKMOV]]{2})", options: .caseInsensitive)

public enum ValidationResponse: Equatable {
    case valid
    case invalid(String)
}

public extension Optional where Wrapped == String {
    func validateName() -> ValidationResponse {
        return (self ?? "").validateName()
    }
    
    func validateEmail() -> ValidationResponse {
        return (self ?? "").validateEmail()
    }
    
    func validatePhoneNumber() -> ValidationResponse {
        return (self ?? "").validatePhoneNumber()
    }
    
    func validatePostcode() -> ValidationResponse {
        return (self ?? "").validatePostcode()
    }
}

public extension String {
    func validateName() -> ValidationResponse {
        // Check is not empty
        if isEmpty {
            return .invalid("Please enter your name")
        }
        
        // Check length
        if count > nameCharacterLimit {
            return .invalid("Maximum \(nameCharacterLimit) characters")
        }
        
        return .valid
    }
    
    func validateEmail() -> ValidationResponse {
        // clean input
        let value = trimmingCharacters(in: .whitespacesAndNewlines)
        
        // Check is not empty
        if value.isEmpty {
            return .invalid("Please enter your email")
        }
        
        // Check length
        if value.count > emailCharacterLimit {
            return .invalid("Maximum \(emailCharacterLimit) characters")
        }
        
        // Check format
        if !value.isEmail() {
            return .invalid("Please make sure your email format is correct")
        }
        
        return .valid
    }
    
    func isEmail() -> Bool {
        return emailRegex.firstMatch(in: self,
                                      options: NSRegularExpression.MatchingOptions(rawValue: 0),
                                      range: NSMakeRange(0, self.count)) != nil // swiftlint:disable:this legacy_constructor
        
    }
    
    func validatePhoneNumber(isMandatory: Bool = true, phoneCharMin: Int = phoneCharacterMinimum, phoneCharMax: Int = phoneCharacterLimit) -> ValidationResponse {
        // clean input
        let value = trimmingCharacters(in: .whitespacesAndNewlines)
        
        // Check is not empty
        if value.isEmpty {
            return isMandatory ? .invalid("Please enter your phone number") : .valid 
        }
        
        // Check format
        if !value.isTelephoneNumber() {
            return .invalid("Please enter a phone number containing only numeric characters and +")
        }
        
        // Check length
        if value.count > phoneCharMax {
            return .invalid("Please enter a phone number with no more than \(phoneCharMax) digits")
        }
        
        if value.count < phoneCharMin {
            return .invalid("Please enter a phone number with at least \(phoneCharMin) digits")
        }
        
        return .valid
    }
    
    func validatePostcode() -> ValidationResponse {
        // clean input
        let value = trimmingCharacters(in: .whitespacesAndNewlines)
        
        // Check is not empty
        if value.isEmpty {
            return .invalid("Please enter your postcode")
        }
        
        // Check format
        if !value.isUKPostcode() {
            return .invalid("Please make sure your postcode is correct")
        }
        
        return . valid
    }
    
    func isTelephoneNumber() -> Bool {
        
        return telephoneRegex.firstMatch(in: self,
                                          options: NSRegularExpression.MatchingOptions(rawValue: 0),
                                          range: NSMakeRange(0, self.count)) != nil // swiftlint:disable:this legacy_constructor
    }
    
    func isUKPostcode() -> Bool {
        return postcodeRegex.firstMatch(in: self,
                                         options: NSRegularExpression.MatchingOptions(rawValue: 0),
                                         range: NSMakeRange(0, self.count)) != nil // swiftlint:disable:this legacy_constructor
    }
    
    func containsOnlyNumbers() -> Bool {
        guard !self.isEmpty else { return false }
        
        let isAllNumbers = self.replacingOccurrences(of: " ", with: "").allSatisfy { $0.isNumber }
        
        return isAllNumbers
    }
}
