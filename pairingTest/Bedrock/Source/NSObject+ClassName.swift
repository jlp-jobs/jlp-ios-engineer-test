//
// Copyright © 2018 John Lewis plc. All rights reserved.
//

import Foundation

extension NSObject {

    public var className: String {
        return "\(type(of: self))"
    }
    
    public class var className: String {
        return "\(self)"
    }
}
