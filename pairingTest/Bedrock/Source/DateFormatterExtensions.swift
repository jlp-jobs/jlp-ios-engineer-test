//
// Copyright © 2018 John Lewis plc. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    /// Date Formats for DateFormatter
    ///
    /// - short: e.g. "11 Dec 2018"
    /// - medium: e.g. "11 December 2018"
    /// - long: e.g. "Tuesday 11 December"
    /// - descriptive: e.g. "11 Dec 2018 at 14:48"
    /// - time: Time with minutes rounded down e.g. "2pm"
    public enum Format: String {
        /// e.g. "11 Dec 2018"
        case short = "dd MMM yyyy"
        /// e.g. "11 December 2018"
        case medium = "dd MMMM yyyy"
        /// e.g. "Tuesday 11 December"
        case long = "EEEE dd MMMM"
        /// e.g. "11 Dec 2018 at 14:48"
        case descriptive = "dd MMM yyyy 'at' HH:mm"
        /// Time with minutes rounded down e.g. "2pm"
        case time = "ha"
    }
    
    /// Initialise a DateFormatter with a specific date format
    ///
    /// - Parameter format: Desired date format
    public convenience init(format: Format) {
        self.init()
        amSymbol = "am"
        pmSymbol = "pm"
        dateFormat = format.rawValue
    }
}
