//
// Copyright © 2019 John Lewis plc. All rights reserved.
//

import Foundation

extension Dictionary {
    public func mapKeys<NewKey>(_ transform: (Key) -> NewKey ) -> [NewKey: Value] {
        var mapped: [NewKey: Value] = [:]
        mapped.reserveCapacity(count)
        forEach { (key: Key, value: Value) in
            let newKey = transform(key)
            mapped[newKey] = value
        }
        return mapped
    }
}
