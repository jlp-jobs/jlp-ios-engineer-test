//
//  Copyright © 2017 John Lewis plc. All rights reserved.
//

import Foundation

extension Decimal {
    
    public func asTwoDecimalPlaceString() -> String {
        
        return String(format: "%.02f", NSDecimalNumber(decimal: self).doubleValue)
    }
}
